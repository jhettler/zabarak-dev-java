package zabarakDev;


import java.awt.Color;

import com.intergraph.tools.utils.disptach.annotations.Action;
import com.intergraph.tools.utils.disptach.annotations.ActionLocation;
import com.intergraph.tools.utils.disptach.annotations.Plugin;
import com.intergraph.web.core.kernel.ApplicationContext;
import com.intergraph.web.core.kernel.plugin.AbstractPlugin;
import com.intergraph.web.viewer.map.GMap;


@Plugin(alias = "zabarakToolsPlugin", vendor = "Intergraph CS s.r.o")
public class BackgroundColorPlugin extends AbstractPlugin
{	
	@Action(actionLocation=ActionLocation.PLUGINTAB, actionOrder=5)
	public void setBackgroundColor()
	{
		GMap map = ApplicationContext.getBrowser().getMap();
		if (map.getBackground().equals(Color.BLACK)) {
			map.setBackground(Color.WHITE);	
		}
		else {
			map.setBackground(Color.BLACK);
		}
		
		map.refresh();
			
	} 
}
