package zabarakDev;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.intergraph.tools.ui.GUIToolkit;
import com.intergraph.tools.utils.disptach.annotations.Action;
import com.intergraph.tools.utils.disptach.annotations.ActionLocation;
import com.intergraph.tools.utils.disptach.annotations.Plugin;
import com.intergraph.tools.utils.log.Log;
import com.intergraph.web.core.data.Project;
import com.intergraph.web.core.data.feature.Feature;
import com.intergraph.web.core.kernel.ApplicationContext;
import com.intergraph.web.core.kernel.plugin.AbstractPlugin;

// Definice pluginu - tvari se jako menu v GMSC
@Plugin(alias = "zabarakToolsPlugin", vendor = "Intergraph CS s.r.o")
// Plugin musi dedit AbstractPlugin
public class SwapFeatureLevelPlugin extends AbstractPlugin {

	// Definice akce (vlastne tlacitka v GMSC, jeho umisteni (ActionLocation), řazení, ikona, ... )
	// Ikony se normalne daji natahnout tak, ze se prida slozka s ikonami do projektu a pak se na ně odkazuje
	// Tahle ikona existuje ve standardnich fcich, takze zadne obrazky nenatahuju
	@Action(icon="arrow_up.svg", actionLocation=ActionLocation.PLUGINTAB, actionOrder=1)
	public void moveOneLevelUp() {
		custSwapFeatureLevel(true, true);
	}

	@Action(icon="arrow_down.svg", actionLocation=ActionLocation.PLUGINTAB, actionOrder=2)
	public void moveOneLevelDown() {
		custSwapFeatureLevel(true, false);
	}

	@Action(icon="arrow_top.svg", actionLocation=ActionLocation.PLUGINTAB, actionOrder=3)
	public void moveToTop() {
		custSwapFeatureLevel(false, true);
	}

	@Action(icon="arrow_bottom.svg", actionLocation=ActionLocation.PLUGINTAB, actionOrder=4)
	public void moveToBottom() {
		custSwapFeatureLevel(false, false);
	}

	// Tady je hlavni metoda, ktera prehazuje vrstvy - pozor na jednu, vrstvy, vizualne ve stromu zustavaji na svem miste
	// meni se jen jejich poradi. Popisu v mailu, jak se to chova
	private void custSwapFeatureLevel(boolean onestep, boolean foreground) {
		//Feature je pro GMSC to, co je v legende jako jedna vrstva
		List<Feature> features = new ArrayList<Feature>();
		Feature activeFeature;

		try {
			// Ted je plugin nastaveny jen nad aktivni vrstvou, nemuzu nejak vycist z dokumentace, jak se dostat
			// k vybranym vrstvam v legende, ale je to na to pripravene - cekam, ze mi dneska nekdo poradi
			// Pak to posune vsechny vybrane vrstvy kam je potreba. Lze posouvat i vrstvy, ktere jsou vybrane
			// pres jejich jmeno nebo simpleID neco jako ApplicationContext.getProject().getFeatureByTitleOrID("302")
			activeFeature = ApplicationContext.getBrowser().getActiveFeature();

			// Tady by melo byt pridani vsech vybranych v legende
			features.add(activeFeature);

			Feature.sortByLevel(features);
			Project project = ApplicationContext.getProject();
			for (Feature f : features) {
				if (onestep) {
					if (foreground) {
						project.setFeatureOneStepToForeground(f.getID());
					} else {
						project.setFeatureOneStepToBackground(f.getID());
					}
				} else if (foreground) {
					project.setFeatureToForeGround(f.getID());
				} else {
					project.setFeatureToBackGround(f.getID());
				}

			}
		} catch (NullPointerException e) {
			String warningText = "Není vybrána aktivní vrstva";
			// Zapisy do logu - tohle je klientsky log, ktery konci s behem aplikace.
			Log.getLogger().log(Level.WARNING, warningText, e);
			// Vyhozeni zakladni hlasky s chybou
			GUIToolkit.showError(String.format(warningText));
		}
	}
}